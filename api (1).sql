-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 04 Cze 2017, 14:28
-- Wersja serwera: 10.1.16-MariaDB
-- Wersja PHP: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `api`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `loan_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `files`
--

INSERT INTO `files` (`id`, `loan_id`, `name`, `url`) VALUES
(44, 14, 'test-pdf.pdf', 'api/files/9fe6f724364652cd99d2d6210b80da4d.pdf'),
(45, 14, 'samle.pdf', 'api/files/204e0f9963b177eb14bdcd438d3c1b23.pdf'),
(46, 15, 'test-pdf.pdf', 'api/files/12fe01ee899799edb35744d10edcfd36.pdf');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `loans`
--

CREATE TABLE `loans` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `fiscal_code` varchar(32) NOT NULL,
  `date` varchar(32) NOT NULL,
  `address` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `loan_type` varchar(32) NOT NULL,
  `total_amount` varchar(32) NOT NULL,
  `rates` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `loans`
--

INSERT INTO `loans` (`id`, `name`, `fiscal_code`, `date`, `address`, `phone`, `loan_type`, `total_amount`, `rates`) VALUES
(14, 'John Doe', '222-333-44-22', '22-10-1992', 'Street 12, 11-222 City', '887 667 886', 'Automotive', '4000.00', '12'),
(15, 'Frank Herbert', '999-888-77-77', '14-06-1978', 'Street 3/12, 33-222 City', '+48 887 667 444', 'Mortgage', '10000', '24');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT dla tabeli `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

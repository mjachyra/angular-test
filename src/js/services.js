(function(){
	'use scrict';

	var BASE_URL = 'http://localhost:4444/api/api.php'
	var app = angular.module('app.services', []);

	app.factory('loansService', ['$http', 'Upload', 'filesService', '$q', function($http, Upload, filesService, $q){
		var _getLoans = function() {

			return $http.post(BASE_URL + '/?action=get_loans')
				.then(function(response){
					if(response.data.success){
						return response.data.data;
					}
				})
		}

		var _getLoanTypes = function(){
			return [
				{
					'name': 'Automotive'
				},
				{
					'name': 'Customer'
				},
				{
					'name': 'Mortgage'
				}
			]
		}

		var _getLoan = function(loanId) {

			return $http.post(BASE_URL + '/?action=get_loan', { id : loanId })
				.then(function(response){
					if(response.data.success){
						return response.data.data;
					}
				})
		}

		var _updateLoan = function(params, callback) {
			callback = callback||function(){};

			return $http.post(BASE_URL + '/?action=update_loan', params)
				.then(function(response){
					if(response.data.success){
						callback();
					}
				})
		}

		var _addLoan = function(params, files, callback) {
			callback = callback||function(){};

			return $http.post(BASE_URL + '/?action=add_loan', params)
				.then(function(response){
					if(response.data.success){
						$q.all([
							angular.forEach(files, function(file){
								filesService.uploadFile({ file: file, id: response.data.loan_id}, function(){
									return true;
								});
							})
						]).then(function(results){
							callback();
						})
						
						
					}
				})
		}

		return {
			getLoans: _getLoans,
			getLoanTypes: _getLoanTypes,
			getLoan: _getLoan,
			updateLoan: _updateLoan,
			addLoan: _addLoan
		}
	}]);


	app.factory('filesService', ['$http', 'Upload', function($http, Upload){
		var _getFiles = function(loanId) {

			return $http.post(BASE_URL + '/?action=get_files', { id : loanId })
				.then(function(response){
					if(response.data.success){
						return response.data.data;
					}
				})
		}

		var _uploadFile = function(params, callback) {
			return Upload.upload({ url: BASE_URL + '/?action=save_file', data: params})
				.then(function(response){
					if(response.data.success){
						callback(response.data);
					}
				})
		}

		var _deleteFile = function(params, callback) {

			return $http.post(BASE_URL + '/?action=delete_file', params)
				.then(function(response){
					if(response.data.success){
						callback(response.data);
					}
				})
		}

		return {
			getFiles: _getFiles,
			uploadFile: _uploadFile,
			deleteFile: _deleteFile
		}
	}]);
})();
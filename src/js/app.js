(function(){
	'use scrict';

	var app = angular.module('loansApp', ['ui.router', 'ngMessages', 'ngAnimate', 'toastr', '720kb.datepicker', 'ngFileUpload', 'app.services', 'app.directives']);

	app.run(function($transitions){
		$transitions.onSuccess({}, function() {
			document.body.scrollTop = document.documentElement.scrollTop = 0;
		});
	});

	app.config(function($urlRouterProvider, $stateProvider, $locationProvider, toastrConfig){
		$stateProvider.state('loans', {
			url: '/',
			templateUrl: 'views/loansList.html',
			resolve: {
				loansData: function(loansService){
					return loansService.getLoans();
				},
				loanTypesData: function(loansService){
					return loansService.getLoanTypes();
				}
			},
			controller: 'loansListCtrl',
		});

		$stateProvider.state('loan', {
			url: '/loans/{loanId}',
			templateUrl: 'views/loanDetails.html',
			resolve: {
				loanData: function(loansService, $transition$){
					return loansService.getLoan($transition$.params().loanId)
				},
				loanFiles: function(filesService, $transition$){
					return filesService.getFiles($transition$.params().loanId)
				}
			},
			controller: 'loanDetailsCtrl'
		});

		$stateProvider
			.state('add', {
				url: '/add',
				template: '<ui-view/>',
				redirectTo: 'add.step-one',
			})
			.state('add.step-one', {
				url: '/step-one',
				templateUrl: 'views/addStepOne.html',
				resolve: {
					loanTypesData: function(loansService){
						return loansService.getLoanTypes();
					}
				},
				controller: 'loanAddStepOneCtrl',
				
			})
			.state('add.step-two', {
				url: '/step-two',
				params: {
					data: null
				},
				templateUrl: 'views/addStepTwo.html',
				resolve: {
					hasParams: function($state, $stateParams){
						if(!$stateParams.data)
							$state.go('add');

						return true;
					}
				},
				controller: 'loanAddStepTwoCtrl',
			})
			.state('add.step-three', {
				url: '/step-three',
				params: {
					data: null
				},
				templateUrl: 'views/addStepThree.html',	
				resolve: {
					hasParams: function($state, $stateParams){
						if(!$stateParams.data)
							$state.go('add');

						return true;
					}
				},
				controller: 'loanAddStepThreeCtrl',
			})

		$urlRouterProvider.otherwise('/');

		$locationProvider.html5Mode(true);

		angular.extend(toastrConfig, {
			closeButton: true,
		    closeHtml: '<button class="close-ico"><i class="fa fa-close"></i></button>',
			templates: {
				toast: 'views/directives/toast.html',
				progressbar: 'views/directives/progressbar.html'
			},
		});
	});

	app.controller('loansListCtrl', ['$scope', 'loansData', 'loanTypesData', function($scope, loansData, loanTypesData){
		$scope.loans = loansData;
		$scope.loanTypes = loanTypesData;
		$scope.filterBy = {};
		$scope.loanTypeFilter = '';

		$scope.filter = function() {
			if($scope.loanTypeFilter === null){
				$scope.filterBy.loan_type = '';	
			}else{
				$scope.filterBy.loan_type = $scope.loanTypeFilter;
			}
	    };
	}]);

	app.controller('loanDetailsCtrl', ['$scope', '$state', 'loansService', 'filesService', 'toastr', 'Upload', 'loanData', 'loanFiles', function($scope, $state, loansService, filesService, toastr, Upload, loanData, loanFiles){
		$scope.loan = loanData;
		$scope.files = loanFiles;

		$scope.notFound = ($scope.loan.length == 0);

		$scope.update = function() {
			if($scope.updateForm.$invalid) return;

			loansService.updateLoan({id : $scope.loan.id, rates : $scope.loan.rates, total_amount : $scope.loan.total_amount }, function(){
				toastr.success('Saved', 'Changes were succesfully saved.');
				$state.go('loans');
			});
		}

		$scope.uploadFile = function(file){
	        if (file) {
	            filesService.uploadFile({id : $scope.loan.id, file : file}, function(data){
	            	$scope.files.push(data.data);
	            })
	        }   
	    }

	    $scope.deleteFile = function(fileId, loanId){
	    	filesService.deleteFile({id : fileId, loan_id : loanId}, function(data){
	    		$scope.files = data.data;
	    	});
	    }
	}]);


	app.controller('loanAddStepOneCtrl', ['$scope', '$state', 'loanTypesData', function($scope, $state, loanTypesData){
		$scope.loanTypes = loanTypesData;
		$scope.loan = {};

		$scope.nextStep = function(){

            if($scope.stepOneForm.$invalid) return;

            $state.go('add.step-two', {
            	data: $scope.loan
            })

        };
	}]);

	app.controller('loanAddStepTwoCtrl', ['$scope', '$state', function($scope, $state){
		$scope.loan = $state.params.data;

		$scope.nextStep = function(){

            if($scope.stepTwoForm.$invalid) return;
            
            $state.go('add.step-three', {
            	data: $scope.loan
            })

        };
	}]);

	app.controller('loanAddStepThreeCtrl', ['$scope', '$state', 'loansService', 'toastr', 'Upload', function($scope, $state, loansService, toastr, Upload){
		$scope.loan = $state.params.data;
		$scope.files = [];

		$scope.addFile = function(file){
	        if (file) {
	            $scope.files.push(file);
	        }   
	    }

	    $scope.deleteFile = function(fileId){
	    	$scope.files.splice(fileId, 1);
	    }

		$scope.save = function(){

            loansService.addLoan($scope.loan, $scope.files, function(){
				toastr.success('Saved', 'New loan has been requested.');
				$state.go('loans');
			});

        };
	}]);

})();
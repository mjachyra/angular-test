(function(){
	'use strict';

	var app = angular.module('app.directives', []);

	app.directive('numberDecimal', function() {

	    return {
	    	restrict: 'A',
			require: 'ngModel',
			link: function(scope, elem, attrs, ctrl) {
				var pattern = new RegExp("^[0-9]+(\.[0-9]{2})?$");
				ctrl.$validators.numberDecimal = function(value) {
					return pattern.test(value);
				};

				scope.$watch('numberDecimal', function() {
					ctrl.$validate();
				});
			}
	    };
	});

	app.directive('integer', function() {

	    return {
	    	restrict: 'A',
			require: 'ngModel',
			link: function(scope, elem, attrs, ctrl) {
				var pattern = new RegExp("^[0-9]*$");
				ctrl.$validators.integer = function(value) {
					return pattern.test(value);
				};

				scope.$watch('integer', function() {
					ctrl.$validate();
				});
			}
	    };
	});

	app.directive('fiscalCode', function() {

	    return {
	    	restrict: 'A',
			require: 'ngModel',
			link: function(scope, elem, attrs, ctrl) {
				var pattern = new RegExp("^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$");
				ctrl.$validators.fiscalCode = function(value) {
					return pattern.test(value);
				};

				scope.$watch('fiscalCode', function() {
					ctrl.$validate();
				});
			}
	    };
	});

	app.directive('toggleSidebar', ['$window', function($window) {
		return {
	    	restrict: 'A',
			link: function(scope, elem, attrs, ctrl) {
				var $sidebar = $('#sidebar');
				var $sidebarHelper = $('#sidebar-helper');

				elem.bind('click', function() {
					if($window.innerWidth < 992){
						$sidebar.toggleClass('active');
						$sidebarHelper.toggleClass('active');
		            }
	            });
			}
	    };
	}]);
})();
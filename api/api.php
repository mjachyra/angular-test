<?php
header('Content-Type: application/json');
$dbhost = "127.0.0.1";
$dbname = "api";
$dbusername = "root";
$dbpassword = "";



if($_GET['action'] == 'get_loans'){
	try {
		$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword, array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));

		$stmt = $pdo->prepare("SELECT * FROM loans");
		$stmt->execute();
		$data = ($stmt->rowCount())? $stmt->fetchAll(PDO::FETCH_ASSOC) : [];
		echo json_encode(array(
			'success' => true,
			'data' => $data,
		));
	} catch (PDOException $e){
		echo json_encode(
			array(
				'success' => false,
				'message' => $e->getMessage(),
			)
		);
	}
}


if($_GET['action'] == 'get_loan'){
	try {
		$params = json_decode(file_get_contents('php://input'),true);
		$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword, array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));

		$stmt = $pdo->prepare("SELECT * FROM loans WHERE id = :id");

		$stmt->bindParam(':id', $params['id'], PDO::PARAM_INT); 
		$stmt->execute();
		$arrayData = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$data = ($stmt->rowCount())? $arrayData[0] : [];
		echo json_encode(array(
			'success' => true,
			'data' => $data,
		));
	} catch (PDOException $e){
		echo json_encode(
			array(
				'success' => false,
				'message' => $e->getMessage(),
			)
		);
	}
}

if($_GET['action'] == 'get_files'){
	try {
		$params = json_decode(file_get_contents('php://input'),true);
		$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword, array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));

		$stmt = $pdo->prepare("SELECT * FROM files WHERE loan_id = :id");

		$stmt->bindParam(':id', $params['id'], PDO::PARAM_INT); 
		$stmt->execute();
		$arrayData = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$data = ($stmt->rowCount())? $arrayData : [];
		echo json_encode(array(
			'success' => true,
			'data' => $data,
		));
	} catch (PDOException $e){
		echo json_encode(
			array(
				'success' => false,
				'message' => $e->getMessage(),
			)
		);
	}
}


if($_GET['action'] == 'update_loan'){
	try {
		$params = json_decode(file_get_contents('php://input'),true);
		$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword, array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));
		$stmt = $pdo->prepare("UPDATE loans SET total_amount = :total_amount, rates = :rates  WHERE id = :id");
		$stmt->bindParam(':id', $params['id'], PDO::PARAM_INT);   
		$stmt->bindParam(':total_amount', $params['total_amount']);   
		$stmt->bindParam(':rates', $params['rates']);   
		$stmt->execute();
		echo json_encode(
			array('success' => true)
		);
	} catch (PDOException $e){
		echo json_encode(
			array(
				'success' => false,
				'message' => $e->getMessage(),
			)
		);
	}
}

if($_GET['action'] == 'add_loan'){
	try {
		$params = json_decode(file_get_contents('php://input'),true);
		$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword, array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));
		$name = $params['name'] . ' ' . $params['surname'];
		$stmt = $pdo->prepare("INSERT INTO loans(name, fiscal_code, date, address, phone, loan_type, total_amount, rates) VALUES(:name, :fiscal_code, :date, :address, :phone, :loan_type, :total_amount, :rates)"); 
		$stmt->bindParam(':name', $name);   
		$stmt->bindParam(':fiscal_code', $params['fiscal_code']);   
		$stmt->bindParam(':date', $params['date']);   
		$stmt->bindParam(':address', $params['address']);   
		$stmt->bindParam(':phone', $params['phone']);   
		$stmt->bindParam(':loan_type', $params['loan_type']);   
		$stmt->bindParam(':total_amount', $params['total_amount']);   
		$stmt->bindParam(':rates', $params['rates']);   
		$stmt->execute();

		$newLoanId = $pdo->lastInsertId();

		echo json_encode(
			array(
				'success' => true,
				'loan_id' => $newLoanId,
			)
		);
	} catch (PDOException $e){
		echo json_encode(
			array(
				'success' => false,
				'message' => $e->getMessage(),
			)
		);
	}
}


if($_GET['action'] == 'save_file'){
	try {
		$info = pathinfo($_FILES['file']['name']);
		$ext = $info['extension'];
		$newname = md5(uniqid(rand(), true)).'.'.$ext;
		$target = 'files/'.$newname;
		$targetUrl = 'api/files/'.$newname;

		$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword, array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));
		$stmt = $pdo->prepare("INSERT INTO files(loan_id, name, url) VALUES(:loan_id, :name, :url)");
		$stmt->bindParam(':loan_id', $_POST['id'], PDO::PARAM_INT);   
		$stmt->bindParam(':name', $info['basename']);   
		$stmt->bindParam(':url', $targetUrl);   
		$stmt->execute();

		$newFileId = $pdo->lastInsertId();

		echo json_encode(
			array(
				'success' => true,
				'data' => array(
					'id' => $newFileId,
					'loan_id' => $_POST['id'],
					'name' => $info['basename'],
					'url' => $targetUrl,
				)
			)
		);

		
		move_uploaded_file( $_FILES['file']['tmp_name'], $target);
	} catch (PDOException $e){
		echo json_encode(
			array(
				'success' => false,
				'message' => $e->getMessage(),
			)
		);
	}
		
}

if($_GET['action'] == 'delete_file'){
	try {
		$params = json_decode(file_get_contents('php://input'),true);
		$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword, array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
		));
		$stmt = $pdo->prepare("DELETE FROM files WHERE id = :id AND loan_id = :loan_id");
		$stmt->bindParam(':id', $params['id'], PDO::PARAM_INT);   
		$stmt->bindParam(':loan_id', $params['loan_id'], PDO::PARAM_INT);   
		$stmt->execute();
		

		$stmt = $pdo->prepare("SELECT * FROM files WHERE loan_id = :loan_id");

		$stmt->bindParam(':loan_id', $params['loan_id'], PDO::PARAM_INT); 
		$stmt->execute();
		$arrayData = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$data = ($stmt->rowCount())? $arrayData : [];
		echo json_encode(array(
			'success' => true,
			'data' => $data,
		));
	} catch (PDOException $e){
		echo json_encode(
			array(
				'success' => false,
				'message' => $e->getMessage(),
			)
		);
	}
}


?>
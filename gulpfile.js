var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var path = require('path');
var fs = require("fs");
var argv = require('yargs').argv;
var isProduction = (function () {
    if (argv._.indexOf('production') !== -1) {
        return true;
    }
    return !!(argv.production)
}());

var onErrorHandler = plugins.notify.onError({
    message: "Error: <%= error.message %>"
});

var srcPaths = {
    js: {
        vendor: [
            './node_modules/angular/angular.js',
            './node_modules/angular-ui-router/release/angular-ui-router.js',
            './node_modules/angular-messages/angular-messages.js',
            './node_modules/angular-animate/angular-animate.js',
            './node_modules/angular-toastr/dist/angular-toastr.js',
            './node_modules/angularjs-datepicker/dist/angular-datepicker.js',
            './node_modules/ng-file-upload/dist/ng-file-upload-shim.js',
            './node_modules/ng-file-upload/dist/ng-file-upload.js',
            './node_modules/jquery/dist/jquery.js',
        ],
        custom: [
            './src/js/services.js',
            './src/js/directives.js',
            './src/js/app.js',
        ]
    }
};

gulp.task('sass', function () {
    return gulp
        .src(['./src/scss/app.scss'])
        .pipe(plugins.if(!isProduction, plugins.sourcemaps.init()))
        .pipe(plugins.sass({
            precision: 8, // Required for bootstrap
            outputStyle: 'expanded'
        }))
        .on('error', onErrorHandler)
        .on('error', plugins.sass.logError)
        .pipe(plugins.autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
        .pipe(plugins.if(isProduction, plugins.cssimport()))
        .pipe(plugins.if(isProduction, plugins.minifyCss()))
        .pipe(plugins.if(!isProduction, plugins.sourcemaps.write()))
        .pipe(gulp.dest('./dist/css/'));

});

gulp.task('js', function () {
    return plugins.merge(
        gulp
            .src(srcPaths.js.vendor)
            .pipe(plugins.if(isProduction, plugins.uglify()))
            .pipe(plugins.concat('vendor.js')),
        gulp
            .src(srcPaths.js.custom)
            .pipe(plugins.if(isProduction, plugins.uglify()))
            .pipe(plugins.concat('custom.js'))
    )
        .pipe(plugins.concat('app.js'))
        .pipe(gulp.dest('./dist/js/'));

});

gulp.task('build', gulp.parallel(
    'js',
    gulp.series(
        'sass'
    )
));

gulp.task('deploy', gulp.series('build'));

gulp.task('watch', function () {

    gulp.watch(['./src/scss/**/*.scss'], gulp.parallel('sass'));

    gulp.watch([
        srcPaths.js.vendor,
        srcPaths.js.custom
    ], gulp.parallel('js'));

});

gulp.task('default', gulp.parallel('build'));
